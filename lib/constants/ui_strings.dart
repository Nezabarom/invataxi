class UiStrings {

  static const String registration = "Регистрация";
  static const String label = 'ИНВАТАКСИ';
  static const String attachPhotoId = 'Пожалуйста прикрепите фото удостовирения';
  static const String attach = 'Прикрепить';
  static const String reference = 'Справка 035-1/у';
  static const String enter = 'Войти';
  static const String register = 'Зарегистрроваться';
  static const String escortTelephone = 'Телефон сопровождения';
  static const String initials = 'ИНН';
  static const String disabilityGroup = 'Группа инвалидности';
  static const String notifications = 'Уведомления';
  static const String news = 'Новость';
  static const String history = 'История';
  static const String feedback = 'Обратня связь';
  static const String status = 'Статус: ';
  static const String selectCategory = 'Выберите категорию';
  static const String complaint = 'Жалоба';
  static const String positiveFeedBAck = 'Положительный отзыв';
  static const String ideasAndSuggestions = 'Идеи и предложения';
  static const String send = 'Отправить';
  static const String writeYourReview = 'Напишите свой отзыв';

}
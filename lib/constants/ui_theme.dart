import 'package:flutter/material.dart';

class UiTheme {
  static const mainColor = Color(0xFF1AB144);
  static const redButtonColor = Color(0xFFFF5661);
  static const lightRedColor = Color(0xFFFF7D77);
  static const yellowButtonColor = Color(0xFFFFE770);
  static const backGroundColor = Color(0xFFF5F1F1);
}
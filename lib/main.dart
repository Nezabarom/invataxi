import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:invataxi/constants/ui_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:invataxi/ui/registration_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      systemNavigationBarColor: UiTheme.mainColor,
      systemNavigationBarDividerColor: Colors.blueGrey,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor: UiTheme.mainColor,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.light,
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return ScreenUtilInit(
        designSize: const Size(360, 690),
        builder: (context, _){
      return const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: RegistrationPage(),
      );
    });
  }
}


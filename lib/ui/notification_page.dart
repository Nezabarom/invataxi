import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:invataxi/constants/ui_strings.dart';
import 'package:invataxi/constants/ui_theme.dart';
import 'package:invataxi/ui/widgets/navigation_drawer.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(UiStrings.notifications),
        backgroundColor: UiTheme.mainColor,
      ),
      drawer: const NavigationDrawer(),
      body: Padding(
        padding: EdgeInsets.only(top: 15.h, right: 15.w, left: 15.w),
        child: Column(
          children: [
            _notificationItem(title: 'Обновлены протоколы безопасности. Упрощен обмен потоковых данных в системе хеширования. Обновлены протоколи розпознования локации.'),
            _notificationItem(title: 'Вас ожидает Алексей на AudiR56'),
          ],
        ),
      ),
    );
  }

  Widget _notificationItem({required String title}){
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10.h, right: 15.w),
              child: Icon(
                Icons.notifications,
                color: UiTheme.mainColor,
                size: 30.w,
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    UiStrings.news,
                    style: TextStyle(fontSize: 20.sp, color: Colors.black87),
                  ),
                  SizedBox(height: 5.h,),
                  Text(
                    title,
                    style: TextStyle(fontSize: 14.sp, color: Colors.grey),
                  ),
                ],
              ),
            )
          ],
        ),
        const Divider(
          height: 15,
          thickness: 1,
          color: Colors.grey,
        )
      ],
    );
  }
}

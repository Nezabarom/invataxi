import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:invataxi/constants/ui_images.dart';
import 'package:invataxi/constants/ui_strings.dart';
import 'package:invataxi/constants/ui_theme.dart';
import 'package:invataxi/ui/widgets/navigation_drawer.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final TextEditingController _phoneController =
  TextEditingController(text: '+7(');
  final TextEditingController _phoneEscortController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  String? selectedValue;
  final List<String> _disabilityGroupList = [
    '| группа',
    '|| группа',
    '||| группа'
  ];
  final FocusNode _phoneFocusNode = FocusNode();
  final FocusNode _phoneEscortFocusNode = FocusNode();
  final FocusNode _nameFocusNode = FocusNode();

  @override
  void dispose() {
    _phoneController.dispose();
    _phoneEscortController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.backGroundColor,
      appBar: AppBar(
        backgroundColor: UiTheme.mainColor,
        title: const Text(UiStrings.registration),
      ),
      drawer: const NavigationDrawer(),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            children: [
              SizedBox(height: 20.h),
              Text(
                UiStrings.label,
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(height: 20.h),
              Text(
                UiStrings.registration,
                style: TextStyle(
                    color: Colors.black87,
                    fontSize: 23.sp,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(height: 15.h),
              _phoneTextField(),
              _phoneEscortTextField(),
              _nameTextField(),
              _pickDisabilityGroup(),
              SizedBox(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 3.h,
                    ),
                    Text(
                      UiStrings.attachPhotoId,
                      style: TextStyle(
                          fontSize: 11.sp,
                          color: Colors.black87
                      ),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Text(
                      UiStrings.attach,
                      style: TextStyle(
                        color: UiTheme.mainColor,
                        fontSize: 11.sp,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Text(
                      UiStrings.reference,
                      style: TextStyle(
                          fontSize: 11.sp,
                          color: Colors.black87
                      ),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Text(
                      UiStrings.attach,
                      style: TextStyle(
                        color: UiTheme.mainColor,
                        fontSize: 11.sp,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.visibility_rounded,
                          size: 30.w,
                        ),
                        Container(
                          height: 30.h,
                          width: 100.w,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.w),
                              color: UiTheme.yellowButtonColor),
                          child: Center(
                              child: Text(
                                UiStrings.enter,
                                style: TextStyle(fontSize: 15.sp),
                              )),
                        ),
                        Container(
                          height: 30.h,
                          width: 170.w,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.w),
                              color: UiTheme.yellowButtonColor),
                          child: Center(
                              child: Text(
                                UiStrings.register,
                                style: TextStyle(fontSize: 15.sp,
                                    color: Colors.black87),
                              )),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Image.asset(UiImages.taxi)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _phoneTextField() {
    return TextFormField(
      controller: _phoneController,
      focusNode: _phoneFocusNode,
      keyboardType: TextInputType.phone,
      textAlign: TextAlign.center,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(top: 13.h),
        hintStyle: TextStyle(fontSize: 14.sp),
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: UiTheme.mainColor),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: UiTheme.mainColor),
        ),
      ),
    );
  }

  Widget _phoneEscortTextField() {
    return TextFormField(
      controller: _phoneEscortController,
      focusNode: _phoneEscortFocusNode,
      keyboardType: TextInputType.phone,
      textAlign: TextAlign.center,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(top: 13.h),
        hintText: UiStrings.escortTelephone,
        hintStyle: TextStyle(color: Colors.black87, fontSize: 14.sp),
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: UiTheme.mainColor),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: UiTheme.mainColor),
        ),
      ),
    );
  }

  Widget _nameTextField() {
    return TextFormField(
      controller: _nameController,
      focusNode: _nameFocusNode,
      keyboardType: TextInputType.text,
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(top: 13.h),
        hintText: UiStrings.initials,
        hintStyle: TextStyle(color: Colors.black87, fontSize: 14.sp),
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: UiTheme.mainColor),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: UiTheme.mainColor),
        ),
      ),
    );
  }

  Widget _pickDisabilityGroup() {
    return Padding(
      padding: EdgeInsets.only(top: 5.h),
      child: Center(
        child: DropdownButtonHideUnderline(
          child: DropdownButtonFormField(
              isExpanded: true,
              value: selectedValue,
              iconEnabledColor: UiTheme.mainColor,
              dropdownColor: UiTheme.backGroundColor,
              style: TextStyle(color: Colors.black87, fontSize: 14.sp),
              decoration: const InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: UiTheme.mainColor),
                ),
                disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: UiTheme.mainColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: UiTheme.mainColor),
                ),
              ),
              hint: Padding(
                padding: EdgeInsets.only(top: 6.h, left: 20.w),
                child: Center(
                    child: Text(
                      UiStrings.disabilityGroup,
                      style: TextStyle(color: Colors.black87, fontSize: 14.sp),
                    )),
              ),
              items: _disabilityGroupList
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Padding(
                    padding: EdgeInsets.only(top: 6.h, left: 20.w),
                    child: Center(child: Text(value)),
                  ),
                );
              }).toList(),
              onChanged: (String? newValue) {
                setState(() {
                  selectedValue = newValue ?? selectedValue;
                });
              }),
        ),
      ),
    );
  }
}

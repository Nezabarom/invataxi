import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:invataxi/constants/ui_images.dart';
import 'package:invataxi/constants/ui_strings.dart';
import 'package:invataxi/constants/ui_theme.dart';
import 'package:invataxi/ui/widgets/navigation_drawer.dart';
import 'package:invataxi/ui/widgets/separator.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({Key? key}) : super(key: key);

  @override
  State<HistoryPage> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  final ScrollController _scrollController = ScrollController();
  late List _listItem;
  int _currentMax = 4;

  @override
  void initState() {
    _listItem = List.generate(_currentMax, (index) => _historyItem);
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  _scrollListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      _getMoreList();
    }
  }

  _getMoreList() {
    for (int i = _currentMax; i < _currentMax + _currentMax; i++) {
      _listItem.add(_historyItem);
    }
    _currentMax = _currentMax + 4;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.backGroundColor,
      appBar: AppBar(
        title: const Text(UiStrings.history),
        backgroundColor: UiTheme.mainColor,
      ),
      drawer: const NavigationDrawer(),
      body: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.builder(
            controller: _scrollController,
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            itemCount: _listItem.length + 1,
            itemBuilder: (context, index) {
              if (index == _listItem.length) {
                return Padding(
                  padding: EdgeInsets.only(bottom: 10.h),
                  child: CupertinoActivityIndicator(
                    radius: 15.w,
                  ),
                );
              }
              return _historyItem();
            }),
      ),
    );
  }

  Widget _historyItem() {
    return Column(
      children: [
        Image.asset(UiImages.map),
        Padding(
          padding: EdgeInsets.only(left: 15.w, right: 15.w, top: 15.h),
          child: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '12 Февраля 2020, 17:00',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 18.sp,
                      color: Colors.black87),
                ),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  children: [
                    Text(
                      UiStrings.status,
                      style: TextStyle(fontSize: 16.sp, color: Colors.grey),
                    ),
                    Text(
                      'ЗАВЕРШЕН',
                      style:
                          TextStyle(fontSize: 16.sp, color: UiTheme.mainColor),
                    )
                  ],
                ),
                SizedBox(
                  height: 5.h,
                ),
                _routeItem(
                    routeColor: UiTheme.mainColor,
                    later: 'A',
                    indicatorVisibleTop: false,
                    indicatorVisibleBot: true,
                    adverb: 'откуда',
                    address: 'Гумара Карамша 33/2',
                    city: 'г. Уральск'),
                _routeItem(
                    routeColor: UiTheme.redButtonColor,
                    later: 'Б',
                    indicatorVisibleTop: true,
                    indicatorVisibleBot: false,
                    adverb: 'куда',
                    address: 'Гагарина 75',
                    city: 'г. Уральск'),

                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20.h),
                  child: const MySeparator(
                    color: Colors.grey,
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _routeItem({
    required Color routeColor,
    required String later,
    required bool indicatorVisibleTop,
    required bool indicatorVisibleBot,
    required String adverb,
    required String address,
    required String city,
  }) {
    return Row(
      children: [
        SizedBox(
          height: 44.h,
          child: Stack(
            alignment: Alignment.center,
            children: [
              (indicatorVisibleTop) ? Padding(
                padding: EdgeInsets.only(bottom: 22.h),
                child: VerticalDivider(
                  width: 6.w,
                  color: Colors.black87,
                  thickness: 1.w,
                ),
              ) :  const SizedBox(),
              (indicatorVisibleBot) ? Padding(
                padding: EdgeInsets.only(top: 22.h),
                child: VerticalDivider(
                  width: 6.w,
                  color: Colors.black87,
                  thickness: 1.w,
                ),
              ) :  const SizedBox(),
              Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: routeColor,
                    border: Border.all(
                      color: Colors.black87,
                      width: 1.w,
                    )),
                width: 7.w,
                height: 7.h,
              ),
            ],
          ),
        ),
        SizedBox(
          width: 15.sp,
        ),
        SizedBox(
          width: 40.w,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                later,
                style: TextStyle(
                  color: routeColor,
                  fontSize: 20.sp,
                ),
              ),
              Text(
                adverb,
                style: TextStyle(
                  color: routeColor,
                  fontSize: 12.sp,
                ),
              )
            ],
          ),
        ),
        SizedBox(
          width: 15.sp,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              address,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 17.sp,
              ),
            ),
            Text(
              city,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 12.sp,
              ),
            )
          ],
        ),
      ],
    );
  }
}


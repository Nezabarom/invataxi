import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:invataxi/constants/ui_strings.dart';
import 'package:invataxi/constants/ui_theme.dart';
import 'package:invataxi/ui/widgets/navigation_drawer.dart';

class FeedBackPage extends StatefulWidget {
  const FeedBackPage({Key? key}) : super(key: key);

  @override
  State<FeedBackPage> createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {

  final TextEditingController _feedBackController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  bool _badFeedBackSwitchValue = false;
  bool _goodFeedBackSwitchValue = false;
  bool _ideaFeedBackSwitchValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(UiStrings.feedback),
        backgroundColor: UiTheme.mainColor,
      ),
      drawer: const NavigationDrawer(),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.only(top: 18.h, right: 15.w, left: 15.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _feedBackTextField(),
              SizedBox(height: 10.h),
              Text(
                UiStrings.selectCategory,
                style: TextStyle(fontSize: 14.sp, color: Colors.grey),
              ),
              SizedBox(height: 10.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    UiStrings.complaint,
                    style: TextStyle(fontSize: 14.sp, color: Colors.black87),
                  ),
                  _badFeedBackSwitch()
                ],
              ),
              SizedBox(height: 10.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    UiStrings.positiveFeedBAck,
                    style: TextStyle(fontSize: 14.sp, color: Colors.black87),
                  ),
                  _goodFeedBackSwitch()
                ],
              ),
              SizedBox(height: 10.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    UiStrings.ideasAndSuggestions,
                    style: TextStyle(fontSize: 14.sp, color: Colors.black87),
                  ),
                  _ideaFeedBackSwitch()
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: _sendButton(),
    );
  }

  Widget _feedBackTextField() {
    return TextFormField(
      controller: _feedBackController,
      focusNode: _focusNode,
      textInputAction: TextInputAction.go,
      maxLines: 10,
      style: TextStyle(
        fontSize: 14.sp,
      ),
      decoration: InputDecoration(
        hintText: UiStrings.writeYourReview,
        hintStyle: TextStyle(fontSize: 14.sp, color: Colors.grey),
        contentPadding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.w),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: Colors.grey.withOpacity(0.5)),
          borderRadius: BorderRadius.circular(4.w),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: Colors.grey.withOpacity(0.5)),
          borderRadius: BorderRadius.circular(4.w),
        ),
      ),
    );
  }

  Widget _sendButton(){
    return InkWell(
      onTap: (){},
      child: Container(
        margin: EdgeInsets.only(bottom: 15.h, right: 15, left: 15),
        width: double.infinity,
        height: 50.h,
        decoration: BoxDecoration(
          color: UiTheme.mainColor,
          borderRadius: BorderRadius.circular(25.w)
        ),
        child: Center(
          child: Text(UiStrings.send, style:  TextStyle(
            color: Colors.white,
            fontSize: 18.sp
          ),),
        ),
      ),
    );
  }

  Widget _badFeedBackSwitch(){
    return SizedBox(
      height: 25.h,
      child: FittedBox(
        child: CupertinoSwitch(
          value: _badFeedBackSwitchValue,
          activeColor: UiTheme.mainColor,
          onChanged: (value) {
            setState(() {
              _badFeedBackSwitchValue = value;
              _ideaFeedBackSwitchValue = false;
              _goodFeedBackSwitchValue = false;
            });
          },
        ),
      ),
    );
  }

  Widget _goodFeedBackSwitch(){
    return SizedBox(
      height: 25.h,
      child: FittedBox(
        child: CupertinoSwitch(
          value: _goodFeedBackSwitchValue,
          activeColor: UiTheme.mainColor,
          onChanged: (value) {
            setState(() {
              _goodFeedBackSwitchValue = value;
              _badFeedBackSwitchValue = false;
              _ideaFeedBackSwitchValue = false;
            });
          },
        ),
      ),
    );
  }

  Widget _ideaFeedBackSwitch(){
    return SizedBox(
      height: 25.h,
      child: FittedBox(
        child: CupertinoSwitch(
          value: _ideaFeedBackSwitchValue,
          activeColor: UiTheme.mainColor,
          onChanged: (value) {
            setState(() {
              _ideaFeedBackSwitchValue = value;
              _badFeedBackSwitchValue = false;
              _goodFeedBackSwitchValue = false;
            });
          },
        ),
      ),
    );
  }
}

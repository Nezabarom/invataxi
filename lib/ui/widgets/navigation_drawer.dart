import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:invataxi/constants/ui_strings.dart';
import 'package:invataxi/constants/ui_theme.dart';
import 'package:invataxi/ui/feed_back_page.dart';
import 'package:invataxi/ui/history_page.dart';
import 'package:invataxi/ui/notification_page.dart';
import 'package:invataxi/ui/registration_page.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(

      width: 298,
      child: SingleChildScrollView(
        child: Column(
          children: [
            _drawerHeader(),
            ColoredBox(
              color: Colors.white,
              child: Column(
                children: [
                  _drawerItem(
                      icon: Icons.app_registration,
                      title: UiStrings.registration,
                      onPressed: () {
                        _onPressItem(context, index: 0);
                      }),
                  _drawerItem(
                      icon: Icons.notifications,
                      title: UiStrings.notifications,
                      onPressed: () {
                        _onPressItem(context, index: 1);
                      }),
                  _drawerItem(
                      icon: Icons.history_edu,
                      title: UiStrings.history,
                      onPressed: () {
                        _onPressItem(context, index: 2);
                      }),
                  _drawerItem(
                      icon: Icons.feedback,
                      title: UiStrings.feedback,
                      onPressed: () {
                        _onPressItem(context, index: 3);
                      }),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _drawerHeader() {
    return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: const BoxDecoration(
          color: UiTheme.mainColor,
          gradient: LinearGradient(
              colors: <Color>[UiTheme.mainColor, Colors.lightGreen])),
      child: Center(
          child: Text(UiStrings.label,
              style: TextStyle(
                letterSpacing: 1.5,
                color: Colors.white,
                fontSize: 24.sp,
                shadows: const <Shadow>[
                  Shadow(
                    offset: Offset(0.0, 0.0),
                    blurRadius: 20,
                    color: Colors.white,
                  ),
                ],
              ))),
    );
  }

  Widget _drawerItem(
      {required IconData icon,
      required String title,
      required Function() onPressed}) {
    return Column(
      children: [
        ListTile(
          onTap: onPressed,
          leading: Icon(icon),
          title: Text(
            title,
            style: TextStyle(fontSize: 14.sp),
          ),
          trailing: const Icon(Icons.arrow_right),
        ),
        const Divider(
          color: Colors.grey,
          height: 0,
        )
      ],
    );
  }

  _onPressItem(BuildContext context, {required int index}) {
    Navigator.pop(context);
    switch (index) {
      case 0:
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const RegistrationPage()));
        break;
      case 1:
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const NotificationPage()));
        break;
      case 2:
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const HistoryPage()));
        break;
      case 3:
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const FeedBackPage()));
        break;
    }
  }
}
